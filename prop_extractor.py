"""extracts type and strength deets"""

import re
# import csv

# with open('files/finalprint ', '.join(row).csv', 'rb') as inputFile:
#     csvreader = csv.reader(inputFile, delimiter=',', quotechar='"')
#     for row in csvreader:
#         print ', '.join(row)


# LINE = "dsdiajs 20mg/ 40 MG"
LINE = "BACTEZOA 200 MG / 500 MG TAB"

def extractor(line):
    """runs regex for type and strength"""
    strength_match = re.search(r'((?!\s)[0-9]+(.[0-9]+)?(\s)*)((M(C?G|L|D)|GMS?|K(G|L))|(%))?(\s)?(\/?)(\s?)((?!\s)[0-9]+(.[0-9]+)?(\s)*)?((M(C?G|L|D)|GMS?|K(G|L))|(%))', line, re.M|re.I)
    # return strength_match

    type_match = re.search(r'(INJ(ECTION)?|TAB(LET)?`?S?|CAP`?S?(ULE)?|POW(DER)?|OINT?(MENT)?|CREAM|LIQ(UID)?|GEL|SY(RU)?P|DROPS?|SUSP(ENSION)?|DURLS?|SPRAY|SOL(UTIO)?N?|SERUM|LOTION)', line, re.M|re.I)

    print "strength: ", strength_match.group()
    print "raw type: ", type_match.group()


extractor(LINE)
