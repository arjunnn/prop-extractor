const fs = require('fs');
const parse = require('csv-parse');
const csv = require('csv-parser');
const async = require('async');
const json2csv = require('json2csv');
let dataArray = [];
const inputFile='../files/meddev002__2017_06_14_14_57_07__dump.csv';

fs.createReadStream(inputFile)
.pipe(csv())
.on('data', function (data) {
  let strengthMatch = new RegExp('((?!\s)[0-9]+(.[0-9]+)?(\s)*)((M(C?G|L|D)|GMS?|K(G|L))|(%))?(\/?)((?!\s)[0-9]+(.[0-9]+)?(\s)*)?((M(C?G|L|D)|GMS?|K(G|L))|(%))', 'g');
  let typeMatch = new RegExp('(INJ(ECTION)?|TAB(LET)?`?S?|CAP`?S?(ULE)?|POW(DER)?|OINT?(MENT)?|CREAM|LIQ(UID)?|GEL|SY(RU)?P|DROPS?|SUSP(ENSION)?|DURLS?|SPRAY|SOL(UTIO)?N?|SERUM|LOTION)', 'g');
  let strength = strengthMatch.exec(data["prod name"]);
  // console.log(data); 
  // console.log(data["prod name"].replace(typeMatch))
  let type = typeMatch.exec(data["prod name"]);
  if(strength !== null) {
    data.strength = strength[0];
  }
  else {
    data.strength = " ";
  }
  if(type !== null) {
    data.type = type[0];
    // let dt = data.type;
    data.type = data.type.replace(/INJ(ECTION)?/, "INJECTION");
    data.type = data.type.replace(/TAB(LET)?`?S?/, "TABLET");
    data.type = data.type.replace(/CAP`?S?(ULE)?/, "CAPSULE");
    data.type = data.type.replace(/POW(DER)?/, "POWDER");
    data.type = data.type.replace(/OINT?(MENT)?/, "OINTMENT");
    data.type = data.type.replace(/LIQ(UID)?/, "LIQUID");
    data.type = data.type.replace(/SY(RU)?P/, "SYRUP");
    data.type = data.type.replace(/DROPS?/, "DROPS");
    data.type = data.type.replace(/SUSP(ENSION)?/, "SUSPENSION");
    data.type = data.type.replace(/DURLS?/, "DURLS");
    data.type = data.type.replace(/SOL(UTIO)?N?/, "SOLUTION");
    // data.type = dt.replace(//, "");
  }
  else {
    data.type = " ";
  }
  // console.log(data);
  dataArray.push(data);
})
.on('end', function(){
  var result = json2csv({ data: dataArray, fields: Object.keys(dataArray[0]) });
  fs.writeFileSync('../files/meddev002__2017_06_14_14_57_07__dump.csv', result);
  console.log("done");
});



let product_names = [
"BRICANYL 2.5MG TAB`S",
"BRICANYL 5MG",
"BRICANYL 5MG DURLS",
"BRICANYL 7.5 DURLS",
"BRICANYL SYP",
"BRICANYL INJ",
"ULMICORT INH 100MD",
"RHINOCORT NASAL SPRAY",
"BRICAREX EXPT",
"BRICAREX EXPT",
"PRIMIPROST",
"CERVIPRIME GEL",
"PROSTODIN INJ 250MG",
"MIT`S LINCTUS-DX LIQ",
"BRICANYL NEBOLIN SOLN",
"RHINOCORT AQUA 64MCG/DOSE",
"CLAVOTROL 375MG TAB`S"
];

// let strengths = [];

// for(let i = 0; i < product_names.length; i++){
//   let strengthMatch = new RegExp('((?!\s)[0-9]+(.[0-9]+)?(\s)*)(M(C?G|L|D)|GMS?)', 'g');
//   let typeMatch = new RegExp('(INJ(ECTION)?|TAB(LET)?`S?|CAPS?(ULE)?|POW(DER)?|OINT(MENT)?|CREAM|LIQ(UID)?|GEL|SY(RU)?P|DROPS?|SUSP(ENSION)?|DURLS?|SPRAY|SOL(UTIO)?N)', 'g');
//   console.log(product_names[i]+"\n======================");
//   let strength = strengthMatch.exec(product_names[i]);
//   let type = typeMatch.exec(product_names[i]);
//   if(strength !== null) {
//     strengths.push(strength[0]);
//     console.log("Strength: "+strength[0]);
//   }
//   else {
//     console.log("Strength: Not detected");
//   }
//   if(type !== null) {
//     console.log("Type: "+type[0]+"\n");
//   }
//   else {
//     console.log("Type: Not detected\n");
//   }
//   // product = str.replace(strength, '');
  
// }
