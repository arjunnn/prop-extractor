# Prop Extractor

This program takes an existing csv sourced from a distributor and then extracts the strength and type values for each medicine and enters them in two new columns, seperately, into a new csv file.

If it is unable to recognise strength, or type, or both, it simple inserts a blank space in the corresponding column.

* Regex used for strength identification:

  [http://regexr.com/3gbfn](http://regexr.com/3gbfn)


* Regex used for type identification:

  [http://regexr.com/3gd88](http://regexr.com/3gd88)

### Examples 

* ```BRICANYL 2.5MG TAB`S```
  
  ```
  **********
  output
  **********
  Strength: 2.5MG
  Type: TAB`S
  ```

* ```VALFECT-H 80/12.5MG TAB`S```
  
  ```
  **********
  output
  **********
  Strength: 80/12.5MG
  Type: TAB`S
  ```

* ```CHYMOTRA SOLUTION 5%```
  
  ```
  **********
  output
  **********
  Strength: 5%
  Type: SOLUTION
  ```